﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Rendering;

public class PortalManager : MonoBehaviour
{
    public GameObject MainCamera;

    public GameObject Sph;

    public GameObject BackPlane;
    public GameObject FrontPlane;



    private Material[] SponzaMaterials;

    private Image if1;


    private Material PortalPlaneMaterial;

    public bool StayIn;

    // Start is called before the first frame update
    void OnEnable()
    {
       ImageController image =  GameObject.FindGameObjectWithTag("ImageController").GetComponent<ImageController>();
        image.portalManager = this;
        image.ImagesList.interactable = true;
        SponzaMaterials = Sph.GetComponent<Renderer>().sharedMaterials;
        PortalPlaneMaterial = GetComponent<Renderer>().sharedMaterial;
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        ChangeTex(image.Images[image.ImagesList.value]);
    }

    private void Update()
    {

    }
    // Update is called once per frame
    void OnTriggerStay(Collider other)
    {
        Vector3 camPositionInPortalSpace = transform.InverseTransformPoint(MainCamera.transform.position);
        if (Mathf.Abs(camPositionInPortalSpace.x) <= 4.4)
        {
            if (Mathf.Abs(camPositionInPortalSpace.y) <= 0.05f)
            {
                for (int i = 0; i < SponzaMaterials.Length; i++)
                {
                    SponzaMaterials[i].SetInt("_StencilComp", (int)CompareFunction.Always);
                }
                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);
                this.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            if (camPositionInPortalSpace.y <= 0.0f)
            {
                for (int i = 0; i < SponzaMaterials.Length; i++)
                {
                    SponzaMaterials[i].SetInt("_StencilComp", (int)CompareFunction.NotEqual);
                }
                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Front);
                this.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                for (int i = 0; i < SponzaMaterials.Length; i++)
                {
                    SponzaMaterials[i].SetInt("_StencilComp", (int)CompareFunction.Equal);
                }
                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Back);
                Sph.SetActive(false);
                this.GetComponent<MeshRenderer>().enabled = true;
            }
            if (camPositionInPortalSpace.y <= -0.1f)
            {

                Sph.SetActive(true);
                BackPlane.SetActive(false);
                FrontPlane.SetActive(true);
            }
            else
            {
                Sph.SetActive(false);
                BackPlane.SetActive(true);
                FrontPlane.SetActive(false);
            }
        }


    }
    float map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
    public void ChangeTex(Texture2D texture)
    {
        for (int i = 0; i < SponzaMaterials.Length; i++)
        {
            SponzaMaterials[i].SetTexture("_MainTex", texture);
        }
    }
}
