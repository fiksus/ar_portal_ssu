﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageController : MonoBehaviour
{
    public Texture2D[] Images;
    public string[] ImagesName;
    public Dropdown ImagesList;

    public PortalManager portalManager;
    // Start is called before the first frame update
    void Start()
    {
        ImagesList.options.Clear();
        foreach (var item in ImagesName)
        {
            ImagesList.options.Add(new Dropdown.OptionData(item));
        }
    }
    public void ChangeImage()
    {
        portalManager.ChangeTex(Images[ImagesList.value]);
    }
}
